<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Ejemplos prácticos de CodeIgniter</title>

	<style type="text/css">

	::selection{ background-color: #E13300; color: white; }
	::moz-selection{ background-color: #E13300; color: white; }
	::webkit-selection{ background-color: #E13300; color: white; }

	body {
		background-color: #fff;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 19px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}

	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	#body{
		margin: 0 15px 0 15px;
	}
	
	p.footer{
		text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}
	
	#container{
		margin: 10px;
		border: 1px solid #D0D0D0;
		-webkit-box-shadow: 0 0 8px #D0D0D0;
	}
	</style>
</head>
<body>

<div id="container">

  <h1>Ejemplos prácticos de CodeIgniter!</h1>
	<div id="body">
		<p>Menú</p>
    <ul>
      <li><a href="/index.php/operacion">Operación</a></li>
        <ul>
          <li><a href="/index.php/operacion/suma">Suma</a></li>
          <li><a href="/index.php/operacion/resta">Resta</a></li>
          <li><a href="/index.php/operacion/multiplicacion">Multiplicación</a></li>
          <li><a href="/index.php/operacion/division">Division</a></li>
        </ul>
      <li><a href="/index.php/area">Área</a></li>
        <ul>
          <li><a href="/index.php/area/cuadrado">Cuadrado</a></li>
          <li><a href="/index.php/area/triangulo">Triangulo</a></li>
          <li><a href="/index.php/area/circulo">Círculo</a></li>
        </ul>
      <li><a href="/index.php/sitio">Sitio</a></li>
        <ul>
          <li><a href="/index.php/sitio/index/es">sitio/index/es</a></li>
          <li><a href="/index.php/sitio/esencia/es">sitio/esencia/es</a></li>
          <li><a href="/index.php/sitio/comedor/es">sitio/comedor/es</a></li>
          <li><a href="/index.php/sitio/index/en">sitio/index/en</a></li>
          <li><a href="/index.php/sitio/esencia/en">sitio/esencia/en</a></li>
          <li><a href="/index.php/sitio/comedor/en">sitio/comedor/en</a></li>
        </ul>
      <li><?php echo anchor("ayudantes", "Ayudantes")?></li>
        <ul>
          <li><?php echo anchor("ayudantes/url", "Helper URL"); ?></li>
          <li><?php echo anchor("ayudantes/formulario", "Helper Form"); ?></li>
        </ul>
    </ul>
	</div>

  <p class="footer">
    <strong>Controlador:</strong> application/controllers/<strong>inicio.php
    <strong>Vista:</strong> application/views/inicio/<strong>vista_index.php</strong>
  </p>

</div>

</body>
</html>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Restaurante Origen Oaxaca</title>
<meta name="description" content="Restaurante Origen Oaxaca es un espacio diseñado y orientado al exquisito goce del paladar. En este lugar de fusionan conceptos gastronómicos como Comedor, Talleres y Compras."/>
<meta name="keywords" content="restaurante origen oaxaca, origenoaxaca, restaurante de autor en oaxaca, chef rodolfo castellanos oaxaca, restaurante rodolfo castellanos oaxaca, mejor restaurante oaxaca."/>
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
<link rel="icon" href="/favicon.ico" type="image/x-icon">
<link href="/recursos/sitio/style.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href='/recursos/sitio/slideshow1.css' type="text/css" />
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script> 
<link rel="stylesheet" href="/recursos/sitio/stylesheet.css" type="text/css" charset="utf-8" />

<style>
*{margin:0; padding:0;}

html{ background:#000 url(/recursos/sitio/imagenes/fondohome.jpg) no-repeat bottom left fixed;
    background-size:cover;
    -moz-background-size:cover;
    -o-background-size:cover;
    -webkit-background-size:cover;
    -khtml-background-size:cover;
}
A:link { text-decoration: none; color:#FFF;}
A:active {text-decoration:underline; color:#666;} 
</style>
<script type="text/javascript">
 
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-24804199-1']);
  _gaq.push(['_trackPageview']);
 
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
 

    function mostrar(div)
    {
        document.getElementById('flotante').style.display = 'none'; /* y repetis esto por cada div, o lo haces automatico */
        document.getElementById('flotante2').style.display = 'none';
        document.getElementById('flotante3').style.display = 'none';
        document.getElementById('flotante4').style.display = 'none';
        document.getElementById(div).style.display = 'block'; /* mostras el que queres mostrar */
    }
</script>
</head>

<body link="#FFFFFF">
<div class="comenu">
<div class="menu"> 
<div class="logo">
  <a href="/index.php/sitio/index/<?php echo $lang; ?>">
    <img src="/recursos/sitio/imagenes/logo.png" width="160" height="165" border="0">
  </a>
</div>

<div class="espacio"></div>

<?php
  $this->load->view($menu);
?>

<div class="espacio2"></div>

<div class="redes">
<a href="https://twitter.com/#!/origenoaxaca" target="_blank"><div class="twitter"></div></a>
<div class="diagonal"></div>
<a href="https://www.facebook.com/pages/Origen-Oaxaca/116393211788018" target="_blank"><div class="face"></div></a>
</div>
</div>
<div class="direccion">
<div class="espacio3"></div>
<font style="font-size:23px">Hidalgo</font> <font style="font-size:32px;">Nº 820</font><br/>
<font style="font-size:10px; text-align:start;">CENTRO HISTÓRICO, OAXACA, MÉXICO</font><br/>
<font style="font-size:18px">TEL. 01 (951) 501 1764</font><br/>
<a href="mailto:contacto@origenoaxaca.com"><font style="font-family: 'Helvetica43ExtendedLight'; font-size:11px; text-align: justify;">contacto@origenoaxaca.com</font><br/></a>
<div class="espacio4"></div>
</div>
</div>

<?php
  $this->load->view($cuerpo);
?>

<?php
  $this->load->view('sitio/vista_pie_pagina');
?>

</body>
</html>

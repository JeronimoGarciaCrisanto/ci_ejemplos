<div class="inf">
<div class="titulo2">
<a href="#"><div id="texto" onClick="mostrar('flotante')" >COMEDOR</div></a>
<a href="#"><div id="textu" onClick="mostrar('flotante2')" >COCINERO</div></a>
<a href="#"><div id="textu" onClick="mostrar('flotante3')">CARTA</div></a>
<a href="#"><div id="textu" onClick="mostrar('flotante4')">PRIVADO</div></a>
</div>

<div id="flotante2" style="display:none;">
<div class="titulo3">
<font style="font-size:18px;">RODOLFO CASTELLANOS</font><br/>
<font style="font-size:14px;">Cocinero Residente</font><br/> 
<font style="font-size:11px; margin-bottom:5px;">rodolfo.castellanos@origenoaxaca.com</font><br/>
</div>
<div class="imagene"><img src="/recursos/sitio/imagenes/RodolfoC 2.jpg" width="245" border="0" height="360"></div>
<div class="textos">
Oaxaqueño de nacimiento, inicia su carrera culinaria siendo un niño inspirado por la cocina de su madre.
Cursa sus estudios profesionales en el Instituto Culinario de México, donde es becado por la Fundación Turquois para realizar una estadía en el principado de Mónaco.<br/>
<br/>
Dentro de su experiencia profesional, destacan participaciones especiales en cocinas de México, Montecarlo y San Francisco. Grandes restaurantes y grandes tutores han dado fuerza y consistencia a cada creación realizada por Rodolfo Castellanos. <br/>




</div>
</div>
<div id="flotante3" style="display:none;">
<div class="titulo4">
<font style="font-size:14px;">Ingredientes, mezclas y sabores de Origen listos para cautivarte.  </font><br/> 
</div>
<div id="rotator">
  <img src="/recursos/sitio/imagenes/carta/imgcarta1.jpg" width="245" border="0" height="360">
  <img src="/recursos/sitio/imagenes/carta/imgcarta2.jpg" width="245" border="0" height="360">
  <img src="/recursos/sitio/imagenes/carta/imgcarta3.jpg" width="245" border="0" height="360">
  <img src="/recursos/sitio/imagenes/carta/imgcarta4.jpg" width="245" border="0" height="360">
  
  </div>
<div class="textos">
<font style="font-size:14px; font-family: 'OswaldRegular';">Entradas<br/>
<br/>
Sopas y Cremas<br/>
<br/>
Platos Fuertes<br/>
<br/>
Ensaladas<br/>
<br/>
Especialidades</font><br/>




</div>
</div>
<div id="flotante4" style="display:none;">
<div class="titulo4">
<a href="mailto:reservaciones@origenoaxaca.com"<font style="font-size:14px;">reservaciones@origenoaxaca.com  </font><br/> 
</div>
<ul class="ppt">
<li><img src="/recursos/sitio/imagenes/privado/imgprivado1.jpg" border="0" width="245" height="360"></li>
<li><img src="/recursos/sitio/imagenes/privado/imgprivado2.jpg" border="0" width="245" height="360"></li>

</ul>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
<script type="text/javascript">
$(".ppt li:gt(0)").hide();
$(".ppt li:last").addClass("last");
var cur = $(".ppt li:first"); function animate() {
cur.fadeOut( 1000 );
if ( cur.attr("class") == "last" )
cur = $(".ppt li:first");
else
cur = cur.next();
cur.fadeIn( 1000 );
} $(function() {
setInterval( "animate()", 5000 );
} );
</script>
<div class="textos">
Las instalaciones de Origen Oaxaca mantienen un espacio privado, en donde las reuniones especiales y eventos, son acompañados de un ambiente exclusivo ubicado en la planta alta de nuestro inmueble.<br/>
<br/>
El privado de Origen está rodeado de arte y decoración digna de una gran celebración. Una sala, un comedor antiguo y una terraza es lo que se puede disfrutar en este espacio.<br/>




</div>
</div>

<div id="flotante">
<div class="imagene"><img src="/recursos/sitio/imagenes/imgcomedor.jpg" width="245" height="360" border="0"></div>
<div class="textos">
En el comedor de Origen Oaxaca, se ofrece una carta especial que mantiene perfecta armonía con el espacio.<br/> 
<br/>
Podemos encontrar platillos especialmente diseñados y elaborados por nuestro cocinero residente Rodolfo Castellanos, quien mantiene un proceso de calidad en la selección de todos los ingredientes.<br/>
<br/>
<br/>


</div>
</div>
</div>

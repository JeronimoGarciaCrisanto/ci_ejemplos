
<div class="inf">
<div class="titulo2">
<a href="#"><div id="texto" onClick="mostrar('flotante')" >DINING SERVICE</div></a>
<a href="#"><div id="textu" onClick="mostrar('flotante2')" >CHEF</div></a>
<a href="#"><div id="textu" onClick="mostrar('flotante3')">MENU</div></a>
<a href="#"><div id="textu" onClick="mostrar('flotante4')">PRIVATE </div></a>
</div>

<div id="flotante2" style="display:none;">
<div class="titulo3">
<font style="font-size:18px;">RODOLFO CASTELLANOS</font><br/>
<font style="font-size:14px;">Resident Chef</font><br/> 
<font style="font-size:11px; margin-bottom:5px;">rodolfo.castellanos@origenoaxaca.com</font><br/>
</div>
<div class="imagene"><img src="/recursos/sitio/imagenes/RodolfoC 2.jpg" width="245" border="0" height="360"></div>
<div class="textos">
Rodolfo, who is a native of Oaxaca, began his culinary career as a child, inspired by his mother’s cookery. He attended the “Instituto Culinario México”, where he received his professional training. During this stage, he was awarded a grant from Turquois Foundation to do an apprenticeship in the Principality of Monaco.<br/>
<br/>
As a professional, he’s made special participations in kitchens throughout Mexico, Montecarlo and San Francisco. Due to the mentorship he received from notable chefs and outstanding restaurants, Rodolfo Castellanos dishes have acquired consistency and strength.  <br/>




</div>
</div>
<div id="flotante3" style="display:none;">
<div class="titulo4">
<font style="font-size:13px;">Origen offers a wide selection of intriguing ingredients, mixtures and flavors.  </font><br/> 
</div>
<div id="rotator">
  <img src="/recursos/sitio/imagenes/carta/imgcarta1.jpg" width="245" border="0" height="360">
  <img src="/recursos/sitio/imagenes/carta/imgcarta2.jpg" width="245" border="0" height="360">
  <img src="/recursos/sitio/imagenes/carta/imgcarta3.jpg" width="245" border="0" height="360">
  <img src="/recursos/sitio/imagenes/carta/imgcarta4.jpg" width="245" border="0" height="360">
  
  </div>
<div class="textos">
<font style="font-size:14px; font-family: 'OswaldRegular';">Entrees<br/>
<br/>
Soups & Creams<br/>
<br/>
Main Courses<br/>
<br/>
Salads<br/>
<br/>
Specialties of the house</font><br/>




</div>
</div>
<div id="flotante4" style="display:none;">
<div class="titulo4">
<a href="mailto:reservaciones@origenoaxaca.com"<font style="font-size:14px;">reservaciones@origenoaxaca.com  </font><br/> 
</div>
<ul class="ppt">
<li><img src="/recursos/sitio/imagenes/privado/imgprivado1.jpg" border="0" width="245" height="360"></li>
<li><img src="/recursos/sitio/imagenes/privado/imgprivado2.jpg" border="0" width="245" height="360"></li>

</ul>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
<script type="text/javascript">
$(".ppt li:gt(0)").hide();
$(".ppt li:last").addClass("last");
var cur = $(".ppt li:first"); function animate() {
cur.fadeOut( 1000 );
if ( cur.attr("class") == "last" )
cur = $(".ppt li:first");
else
cur = cur.next();
cur.fadeIn( 1000 );
} $(function() {
setInterval( "animate()", 5000 );
} );
</script>
<div class="textos">
The space in Origen Oaxaca is designed to maintain the privacy of our clients. Special events and celebrations are held in an exclusive room located on the second floor of the building.<br/>
<br/>
Origen’s private room is surrounded by decorative arts creating a festive atmosphere suitable for any event. Inside this exclusive space, clients also have access to the living room, a vintage dining room and the tranquility of the terrace. <br/>




</div>
</div>

<div id="flotante">
<div class="imagene"><img src="/recursos/sitio/imagenes/imgcomedor.jpg" width="245" height="360" border="0"></div>
<div class="textos">
The menu that Origen Oaxaca’s dining service offers, keeps balance with the harmonic environment of the location.<br/> 
<br/>
Our dishes are meticulously designed and cooked by our resident Chef, Rodolfo Castellanos, who chooses the ingredients with utmost care.<br/>
<br/>
<br/>


</div>
</div>
</div>

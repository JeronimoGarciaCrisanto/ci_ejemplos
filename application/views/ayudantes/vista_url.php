<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Helper URL</title>
</head>

<body>

<h1>Helper URL</h1>

<p>
  A continuación se enlistan algunas de las funciones disponibles en el Helper URL
</p>

<ul>
  <li>site_url() => <?php echo site_url(); ?> </li>
  <li>site_url("controlador/accion") => <?php echo site_url("controlador/accion"); ?> </li>
  <li>base_url() => <?php echo base_url(); ?> </li>
  <li>current_url() => <?php echo current_url(); ?> </li>
  <li>uri_string() => <?php echo uri_string(); ?> </li>
  <li>anchor() => <?php echo anchor(); ?> </li>
  <li>anchor("controlador/accion") => <?php echo anchor("controlador/accion"); ?> </li>
  <li>anchor("operacion/suma/12/23") => <?php echo anchor("operacion/suma/12/23"); ?> </li>
  <li>anchor("operacion/suma/12/23", "12 + 23") => <?php echo anchor("operacion/suma/12/23", "12 + 23"); ?> </li>
  <li>anchor("inicio/index", "Inicio") => <?php echo anchor("inicio/index", "Inicio"); ?> </li>
  <li>anchor_popup('operacion/index', 'Operaciones', $atributos) => 
<?php
  $atributos = array(
              'width'      => '800',
              'height'     => '600',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'yes',
              'screenx'    => '0',
              'screeny'    => '0'
            );
  echo anchor_popup('operacion/index', 'Operaciones', $atributos);
?>
  </li>
  <li>safe_mailto("micorreo@dominio.com", "Contactame") => <?php echo safe_mailto("micorreo@dominio.com", "Contactame"); ?></li>
  <li>auto_link($cadena) =>
<?php
  $cadena = "Esta es una cadena con un vínculo a http://google.com.mx y correo electrónico micorreo@dominio.com";
  echo auto_link($cadena);
?>
</ul>

</body>
</html>

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sitio extends CI_Controller
{
  private $datos;
   
  public function __construct()
  {
    parent::__construct();
    $this->datos = array(
      'menu' => '',
      'lang' => '',
      'cuerpo' => '',
    );
  }
  
  public function index($lang = "es")
  {
    $this->_setLang($lang);
    $this->datos['menu'] = 'sitio/'.$this->datos['lang'].'/vista_menu';
    $this->datos['cuerpo'] = 'sitio/'.$this->datos['lang'].'/vista_index';
    $this->load->view('sitio/vista_base.php', $this->datos);
  }

  public function esencia($lang = "es")
  {
    $this->_setLang($lang);
    $this->datos['menu'] = 'sitio/'.$this->datos['lang'].'/vista_menu';
    $this->datos['cuerpo'] = 'sitio/'.$this->datos['lang'].'/vista_esencia';
    $this->load->view('sitio/vista_base.php', $this->datos);
  }

  public function comedor($lang = "es")
  {
    $this->_setLang($lang);
    $this->datos['menu'] = 'sitio/'.$this->datos['lang'].'/vista_menu';
    $this->datos['cuerpo'] = 'sitio/'.$this->datos['lang'].'/vista_comedor';
    $this->load->view('sitio/vista_base.php', $this->datos);
  }
 
  private function _setLang($lang) {
    if ($lang == "en" || $lang == "es") {
      $this->datos['lang'] = $lang;
    } else {
      $this->datos['lang'] = "es";
    }
  }

}

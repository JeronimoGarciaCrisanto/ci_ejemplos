<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Operacion extends CI_Controller
{

  private $datos;
   
  public function __construct()
  {
    parent::__construct();
    $this->datos = array(
      'titulo_principal' => '',
      'mensaje' => '',
    );
  }
  
  public function index()
  {
    $this->datos['titulo_principal'] = "Operación";
    $this->datos['mensaje'] = "Escoge una de las operaciones del menú";
    $this->load->view('operacion/vista_index.php', $this->datos);
  }
 
  public function suma($x = 0, $y = 0)
  {
    $this->datos['titulo_principal'] = "Resultado de la Suma";

    if (is_numeric($x) && is_numeric($y)) {
      $this->datos['mensaje'] = ($x + $y);
    } else {
      $this->datos['mensaje'] = "Solo se puede sumar números.";
    }

    $this->load->view('operacion/vista_resultado.php', $this->datos);
  }
 
  public function resta($x = 0, $y = 0)
  {
    $this->datos['titulo_principal'] = "Resultado de la Resta";

    if (is_numeric($x) && is_numeric($y)) {
      $this->datos['mensaje'] = ($x - $y);
    } else {
      $this->datos['mensaje'] = "Error, solo se puede restar números.";
    }
    $this->load->view('operacion/vista_resultado.php', $this->datos);
  }
 
  public function multiplicacion($x = 0, $y = 0)
  {
    $this->datos['titulo_principal'] = "Resultado de la Multiplicación";

    if (is_numeric($x) && is_numeric($y)) {
      $this->datos['mensaje'] = ($x * $y);
    } else {
      $this->datos['mensaje'] = "Error, solo se puede multiplicar números.";
    }
    $this->load->view('operacion/vista_resultado.php', $this->datos);
  }
 
  public function division($x = 0, $y = 1)
  {
    $this->datos['titulo_principal'] = "Resultado de la División";

    if (is_numeric($x) && is_numeric($y)) {
      if ($y != 0) {
        $this->datos['mensaje'] = ($x / $y);
      } else {
        $this->datos['mensaje'] = "Error, divisor igual a 0.";
      }
    } else {
      $this->datos['mensaje'] = "Error, solo se puede dividir números.";
    }
    $this->load->view('operacion/vista_resultado.php', $this->datos);
  }

}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* Este controlador es el que por default usará CodeIgniter.
 * Guía de Usuario: pág. 38 - Definir un Controlador por Defecto
 */
class Inicio extends CI_Controller {

	public function index()
	{
    /* La vista se encuentra en application/views/inicio/vista_index.php
     * Guía de Usuario: pág 44 - Almacenar Vistas dentro de Subcarpetas
     */
    $this->load->view('inicio/vista_index');
	}

}
